﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddAppointment.xaml
    /// </summary>
    public partial class AddAppointment : Page
    {
		public int Number { get; set; }
        public AddAppointment(string str)
        {
            InitializeComponent();
            DB.DBConn conn = new DB.DBConn();            
            List<DB.Service> services = conn.Service.ToList();
            this.Service.ItemsSource = services;
            Service.DisplayMemberPath = "Name";
            
            List<DB.StatusComplete> statusCompletes = conn.StatusComplete.ToList();
            this.StatusComplete.ItemsSource = statusCompletes;
            StatusComplete.DisplayMemberPath = "Name";

			DB.Appointment appointment = new DB.Appointment();
			DB.Patient patient = conn.Patient.Where(u => u.Name == str).FirstOrDefault();			
			Patient.Text = str;
			Number = patient.ID;			
		}
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var b = int.Parse(ID.Text);
            DB.DBConn conn = new DB.DBConn();
            DB.Appointment appointment = new DB.Appointment();			

            appointment.ID = b;            
            appointment.Service = ((DB.Service)Service.SelectedItem).ID;
            appointment.Date = txtDate.SelectedDate.Value;
            appointment.StatusComplete = ((DB.StatusComplete) StatusComplete.SelectedItem).Name;
			appointment.Barcode = txtBarcode.Text;
			appointment.Patient = Number;

            conn.Appointment.Add(appointment);
            conn.SaveChanges();

            ID.Clear();			
			Service.SelectedIndex = -1;
			StatusComplete.SelectedIndex = -1;
			txtBarcode.Clear();
			txtDate.Text = null;
			Barcode.Children.Clear();
        }		
        private void Button_Click(object sender, RoutedEventArgs e)
        {			
			NavigationService.Navigate(new Pages.Patient(Number));
        }
        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
			Random random = new Random();
			txtBarcode.Text = $"{txtDate.SelectedDate.Value.Day}{txtDate.SelectedDate.Value.Month}{txtDate.SelectedDate.Value.Year}{((DB.Service)Service.SelectedItem).ID}{random.Next(9999)}";
			DrawBarcode(txtBarcode.Text);
        }
		public void DrawBarcode(string code)
		{			
			double width;
			double startLine = 25;
			double height;

			TextBlock[] labels = new TextBlock[12];
			try
			{
				for (int index = 0; index < code.Length; index++)
				{
					int digit = int.Parse(code.Substring(index, 1));

					Line line = new Line();

					if (digit == 0)
					{
						width = 13.5;
						line.StrokeThickness = width;
						line.Stroke = Brushes.Pink;
					}
					else
					{
						width = 1.5 * digit;
						line.StrokeThickness = width;
						line.Stroke = Brushes.Black;
					}

					if (index == 0 || index == 6 || index == 12)
					{
						height = 105;						
					}
					else
					{
						height = 85;
					}

					line.X1 = startLine;
					line.Y1 = 20;
					line.X2 = startLine;
					line.Y2 = height;

					labels[index] = new TextBlock();
					labels[index].Width = 10;
					labels[index].Height = 20;
					labels[index].Name = "label" + index.ToString();
					labels[index].Text = code[index].ToString();
					labels[index].Foreground = Brushes.Black;
					labels[index].FontSize = 15;

					Canvas.SetLeft(labels[index], startLine);
					Canvas.SetTop(labels[index], height);

					startLine += width + 7;
					Barcode.Children.Add(labels[index]);
					Barcode.Children.Add(line);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}
