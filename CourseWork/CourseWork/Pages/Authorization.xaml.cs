﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для Authorization.xaml
    /// </summary>
    public partial class Authorization : Page
    {
        public Authorization()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DB.DBConn connection = new DB.DBConn();
            DB.Employee employee = connection.Employee.Where(u => u.Login == txtLog.Text && u.Password == txtPass.Password).FirstOrDefault();
            DB.Patient patient = connection.Patient.Where(u => u.Login == txtLog.Text && u.Password == txtPass.Password).FirstOrDefault();

            if(employee != null)
            {
                if (employee.Login == txtLog.Text && employee.Password == txtPass.Password && employee.Role == "admin")
                {
                    NavigationService.Navigate(new Pages.Admin(employee.ID));
                }
                if (employee.Login == txtLog.Text && employee.Password == txtPass.Password && employee.Role == "doctor")
                {
                    NavigationService.Navigate(new Pages.Doctor(employee.ID));
                }
            }
            if(patient != null)
            {
                if (patient.Login == txtLog.Text && patient.Password == txtPass.Password)
                {
                    NavigationService.Navigate(new Pages.Patient(patient.ID));
                }
            }            
        }
    }
}
