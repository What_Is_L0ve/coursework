﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для Patient.xaml
    /// </summary>
    public partial class Patient : Page
    {
        public Patient(int id)
        {
            InitializeComponent();
            DB.DBConn conn = new DB.DBConn();
            DB.Patient patient = conn.Patient.Where(u => u.ID == id).FirstOrDefault();
            Name.Text = patient.Name.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Pages.AddAppointment(Name.Text));
        }
    }
}
