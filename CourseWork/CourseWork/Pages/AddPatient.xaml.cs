﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddPatient.xaml
    /// </summary>
    public partial class AddPatient : Page
    {
        public AddPatient()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var b = int.Parse(ID.Text);
            DB.DBConn conn = new DB.DBConn();
            DB.Patient patient = new DB.Patient();

            patient.ID = b;
            patient.Name = Name.Text;
            patient.Phone = Phone.Text;
            patient.SerialPassport = SerialPassport.Text;
            patient.NumberPassport = NumberPassport.Text;
            patient.Login = Login.Text;
            patient.Password = Password.Text;
            patient.DateBorn = DateBorn.SelectedDate.Value;

            conn.Patient.Add(patient);
            conn.SaveChanges();

            ID.Clear();
            Name.Clear();
            Phone.Clear();
            SerialPassport.Clear();
            NumberPassport.Clear();
            Login.Clear();
            Password.Clear();
            DateBorn.Text = null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DB.DBConn conn = new DB.DBConn();
            DB.Employee employee = conn.Employee.Where(u => u.Role == "admin").FirstOrDefault();
            NavigationService.Navigate(new Pages.Admin(employee.ID));
        }
    }
}
