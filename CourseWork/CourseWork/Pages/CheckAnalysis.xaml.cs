﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork.Pages
{
    /// <summary>
    /// Логика взаимодействия для CheckAnalysis.xaml
    /// </summary>
    public partial class CheckAnalysis : Page
    {
        public CheckAnalysis()
        {
            InitializeComponent();
            DB.DBConn conn = new DB.DBConn();
            List<DB.Appointment> appointments = conn.Appointment.ToList();
            this.Appointment.ItemsSource = appointments;
            Appointment.DisplayMemberPath = "Patient";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var b = int.Parse(ID.Text);
            var a = int.Parse(CountMicrobody.Text);
            DB.DBConn conn = new DB.DBConn();
            DB.Analysis analysis = new DB.Analysis();

            analysis.ID = b;
            analysis.Appointment = ((DB.Appointment)Appointment.SelectedItem).ID;
            analysis.CountMicrobody = a;

            if (a > 750)
            {
                analysis.Result = true;
            }
            else
            {
                analysis.Result = false;
            }

            conn.Analysis.Add(analysis);
            conn.SaveChanges();

            ID.Clear();
            Appointment.SelectedIndex = -1;
            Result.Clear();
            CountMicrobody.Clear();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DB.DBConn dBConn = new DB.DBConn();
            DB.Employee employee = dBConn.Employee.Where(u => u.Login == "v" && u.Password == "v").FirstOrDefault();
            NavigationService.Navigate(new Pages.Doctor(employee.ID));
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            var rand = random.Next(1000);

            if (rand > 750)
            {
                Result.Text = "true";
            }
            else
            {
                Result.Text = "false";
            }

            CountMicrobody.Text = rand.ToString();
        }
    }
}
